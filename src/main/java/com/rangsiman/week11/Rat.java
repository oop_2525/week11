package com.rangsiman.week11;

public class Rat extends Animal implements Walkable, Swimable{

    public Rat(String name) {
        super(name, 4);
    }

    @Override
    public String toString() {
        return "Rat ("+getName()+")";
    }
    
    @Override
    public void sleep() {
        System.out.println(this+" sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this+" eat.");
    }

    @Override
    public void walk() {
        System.out.println(this+" walk.");
    }

    @Override
    public void run() {
        System.out.println(this+" run.");
    }

    @Override
    public void swim() {
        System.out.println(this+" swim.");
    }
}
