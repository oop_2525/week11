package com.rangsiman.week11;

public class Bird extends Animal implements Flyable, Walkable{

    public Bird(String name) {
        super(name, 2);
    }

    @Override
    public String toString() {
        return "Bird ("+getName()+")";
    }

    @Override
    public void sleep() {
        System.out.println(this+" sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this+" eat.");
    }

    @Override
    public void takeoff() {
        System.out.println(this+" takeoff.");
    }

    @Override
    public void fly() {
        System.out.println(this+" fly.");
    }

    @Override
    public void landing() {
        System.out.println(this+" landing.");
    }

    @Override
    public void walk() {
        System.out.println(this+" walk.");
    }

    @Override
    public void run() {
        System.out.println(this+" run.");
    }
}
