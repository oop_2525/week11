package com.rangsiman.week11;

public class App 
{
    public static void main( String[] args )
    {
        Bird bird = new Bird("Tweety");
        bird.sleep();
        bird.eat();
        bird.walk();
        bird.run();
        bird.takeoff();
        bird.fly();
        bird.landing();
        System.out.println(" ");

        Plane plane = new Plane("Boeing-69", "Engine-30");
        plane.takeoff();
        plane.fly();
        plane.landing();
        System.out.println(" ");

        Human human = new Human("Adam");
        human.sleep();
        human.eat();
        human.walk();
        human.run();
        human.swim();
        System.out.println(" ");

        Superman clark = new Superman("Clark");
        clark.sleep();
        clark.eat();
        clark.walk();
        clark.run();
        clark.swim();
        clark.takeoff();
        clark.fly();
        clark.landing();
        System.out.println(" ");

        Snake snake = new Snake("Peter");
        snake.sleep();
        snake.eat();
        snake.crawl();
        System.out.println(" ");

        Dog dog = new Dog("Doge");
        dog.sleep();
        dog.eat();
        dog.walk();
        dog.run();
        dog.swim();
        System.out.println(" ");

        Cat cat = new Cat("Miso");
        cat.sleep();
        cat.eat();
        cat.walk();
        cat.run();
        cat.swim();
        System.out.println(" ");

        Rat rat = new Rat("Ratatouille");
        rat.sleep();
        rat.eat();
        rat.walk();
        rat.run();
        rat.swim();
        System.out.println(" ");

        Fish fish = new Fish("William");
        fish.sleep();
        fish.eat();
        fish.swim();
        System.out.println(" ");

        Crocodile crocodile = new Crocodile("Paolo");
        crocodile.sleep();
        crocodile.eat();
        crocodile.walk();
        crocodile.run();
        crocodile.swim();
        System.out.println(" ");

        Flyable[] flyables = {bird, plane, clark};
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        System.out.println(" ");

        Walkable[] walkables = {bird, human, clark, dog, cat, rat, crocodile};
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }
        System.out.println(" ");

        Swimable[] swimables = {human, clark, dog, cat, rat, fish, crocodile};
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }
        System.out.println(" ");

    }
}
