package com.rangsiman.week11;

public class Snake extends Animal implements Crawlable{

    public Snake(String name) {
        super(name, 0);
    }

    @Override
    public String toString() {
        return "Snake ("+getName()+")";
    }

    @Override
    public void sleep() {
        System.out.println(this+" sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this+" eat.");
    }

    @Override
    public void crawl() {
        System.out.println(this+" crawl.");        
    }
}
