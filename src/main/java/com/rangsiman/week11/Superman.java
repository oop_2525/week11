package com.rangsiman.week11;

public class Superman extends Human implements Flyable{

    public Superman(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Superman ("+getName()+")";
    }

    @Override
    public void takeoff() {
        System.out.println(this+" takeoff.");
    }

    @Override
    public void fly() {
        System.out.println(this+" fly.");
    }

    @Override
    public void landing() {
        System.out.println(this+" landing.");
    }
}
